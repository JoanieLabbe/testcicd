---
title: front matter
key: value
---

# H1

## H2

### H3

#### H4

##### H5

###### H6



Lorem *ipsum* dolor sit amet, graecis denique ei vel, at duo primis mandamus. Et legere ocurreret pri, animal tacimates complectitur ad cum. Cu eum inermis inimicus efficiendi. Labore _officiis_ his ex, soluta officiis concludaturque ei qui, vide ~~sensibus~~ vim ad.

* de quoi

* de quoi d'autre

* Allô
  
  ![](/home/joanie/Téléchargements/test.gif)

> Blockquotes <br>joie dans mon cœur

+ raconter sa vie
  
  + j'aimerais avoir de quoi à raconter



1. Comment être heureux

2. Passer du temps avec les gens que l'on aime



- [ ] truc à faire 1

- [ ] truc à faire 2



| Tableau | Description       |
| ------- | ----------------- |
| Joie    | J'aime faire ça   |
| Bon     | Vraiment beaucoup |

```html
<html>

    <head>
        </br>
    </head>

</html>
```

<kbd>Ctrl</kbd>+<kbd>C</kbd>

:heart:

$$
m=\frac{b_y-a_y}{b_x-a_x}
$$

```

```



```mermaid
graph TD;
    A-->B
    A-->C
    B-->D
    C-->D
```







```mermaid
sequenceDiagram
    participant Alice
    participant Bob
Alice->>John: Hello John, how are you
loop Healthcheck
    John->>John: Fight against hypochondria
end
Note right of John: Some note
John-->>Alice: Great !
John->>Bob: How about you?
Bob-->>John: Jolly good!
```



```mermaid

```