using Microsoft.VisualStudio.TestTools.UnitTesting;
using testCICD;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            BasicMath math = new BasicMath();
            int result = math.Addition(2, 2);
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void TestMethodInvalid()
        {
            BasicMath math = new BasicMath();
            int result = math.Addition(0, -2);
            Assert.AreEqual(-2, result);
        }
    }
}
